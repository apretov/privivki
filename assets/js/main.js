// Include for header, footer, etc
function includeHTML() {
	var z, i, elmnt, file, xhttp;
	/* Loop through a collection of all HTML elements: */
	z = document.getElementsByTagName("*");
	for (i = 0; i < z.length; i++) {
		elmnt = z[i];
		/*search for elements with a certain atrribute:*/
		file = elmnt.getAttribute("include");
		if (file) {
			/* Make an HTTP request using the attribute value as the file name: */
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (this.readyState == 4) {
					if (this.status == 200) {
						elmnt.innerHTML = this.responseText;
						// get the element's parent node
						var parent = elmnt.parentNode;
						// move all children out of the elmntement
						while (elmnt.firstChild) parent.insertBefore(elmnt.firstChild, elmnt);
						// remove the empty element
						parent.removeChild(elmnt);
					}
					if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
					/* Remove the attribute, and call this function once more: */
					elmnt.removeAttribute("include");
					includeHTML();
				}
			} 
			xhttp.open("GET", file, true);
			xhttp.send();
			/* Exit the function: */
			return;
		}
	}
}
includeHTML();


// Modals
function openModal(id) {
	closeModals();
	document.getElementById(id).classList.remove('modal-hidden');
	document.querySelector('.overlay').classList.remove('overlay-hidden');
}

function closeModals() {
	let modals = document.querySelectorAll('.modal');

	for (var i = modals.length - 1; i >= 0; i--) {
		modals[i].classList.add('modal-hidden');
	}

	document.querySelector('.overlay').classList.add('overlay-hidden');
}